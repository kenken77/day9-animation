(function() {
    "use strict";
    angular.module("AnimateApp").controller("AnimateCtrl", AnimateCtrl);

    AnimateCtrl.$inject = ['$http', '$log'];

    function AnimateCtrl($http, $log) {
        self = this;
        self.helloworld = "";
        self.boxClass = true;
        self.circleClass = true;
        self.triangleClass = false;

        self.disableBox = function() {
            self.boxClass = false;
            self.circleClass = false;
        };
        $http.get("/hello").then(function(response) {
            $log.info(response.data);
            $log.debug(response.data);
            console.log("hello");
            self.helloworld = response.data;
        }).catch(function(error) {
            $log.error(error);
        });
    }

})();